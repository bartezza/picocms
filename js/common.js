
function setIntervalAndExecute(fn, t) {
    fn();
    return(setInterval(fn, t));
}

function refreshContent(node, url, doRefresh) {
    var times = 3000;
    setIntervalAndExecute(
        function() {
            /*      var address;
            if(node.src.indexOf('?')>-1)
                address = node.src.split('?')[0];
            else 
                address = node.src;
            node.src = address+\"?time=\"+new Date().getTime();*/
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4) {
                    if (this.status == 200) {
                        node.innerHTML = this.responseText;
                    } else {
                        node.innerHTML = "readyState = " + this.readyState + ", status = " + this.status;
                    }
                    //if (doRefresh == true) {
                    //    setTimeout(startRefreshContent, times);
                    //}
                }
            };
            xhttp.open("GET", url, true);
            xhttp.send();
            //node.innerHTML = '$html';
            //setTimeout(startRefreshContent, times);
        },
        times
    );
}

function refreshUrl(node, url, doRefresh) {
    var times = 10000;
    setInterval(
        function() {
            /*var address;
            if(node.src.indexOf('?')>-1)
                address = node.src.split('?')[0];
            else 
                address = node.src;
            node.src = address+\"?time=\"+new Date().getTime();*/
            node.src = url + "&time=" + new Date().getTime();
            //if (doRefresh == true) {
            //    setTimeout(startRefresh,times);
            //}
        }, times);
}

//! NOTE: parentElem should be a d3 element
function addDialogBox(parentElem, title, message, classname) {
    var theDiv = parentElem.append('div');
    theDiv.attr('class', 'dialog_box ' + classname)
        .append('h2')
        .text(title);
    theDiv.append('div')
        .attr('class', 'dialog_box_cont')
        .html(message);
}

//! NOTE: parentElem should be a d3 element
function addWarningBox(parentElem, title, message) {
    addDialogBox(parentElem, title, message, 'warning_box');
}

//! NOTE: parentElem should be a d3 element
function addErrorBox(parentElem, title, message) {
    addDialogBox(parentElem, title, message, 'error_box');
}
