<?php
/*
    User account class
*/

require_once __DIR__ . "/config.php";
require_once __DIR__ . "/common.php";

class User {
    
    //! Boolean: is the current user admin?
    var $m_isAdmin;
    var $m_username;
    var $m_userId;
    
    //! Boolean: is the current user logged in?
    var $m_isLoggedIn;
    
    //! String holding error related to a form (such as the login form)
    var $m_formError;

    //! The login page
    var $m_loginPage;
    
    function __construct() {
        // check session
        $this->checkSession();
        // check actions
        $this->checkActions();
    }
 
    //! Reset internal user data
    private function reset() {
        $this->m_isLoggedIn = false;
        $this->m_isAdmin = false;
        $this->m_username = "";
        $this->m_userId = -1;
    }

    //! Logout the user and unset session stuff
    function logout() {    
        $this->reset();
        unset($_SESSION['isLoggedIn']);
        unset($_SESSION['user']);
    }
    
    //! Check session
    function checkSession() {
        $this->reset();
        // check if logged in
        if (isset($_SESSION['isLoggedIn']) == true && $_SESSION['isLoggedIn'] === true) {
            // check user stuff from session
            if (isset($_SESSION['user'])) {
                $this->m_username = $_SESSION['user']['username'];
                $this->m_userId = $_SESSION['user']['userId'];
                $this->m_isAdmin = $_SESSION['user']['isAdmin'];
                $this->m_isLoggedIn = true;
            }
        }
    }
    
    //! Update session info
    function updateSession() {
        if ($this->m_isLoggedIn) {
            $_SESSION["user"] = array(
                "username" => $this->m_username,
                "userId" => $this->m_userId,
                "isAdmin" => $this->m_isAdmin
            );
            $_SESSION["isLoggedIn"] = true;
        } else {
            unset($_SESSION["isLoggedIn"]);
            unset($_SESSION["user"]);
        }
    }
    
    //! Login (to be overriden)
    function login($username, $password) {
        raiseFatalError("User::login()", "To be overriden!");
    }
    
    //! Check login/logout actions
    function checkActions() {
        // logging in?
        if (isAction("login") == true) {
            // check password
            if (isset($_REQUEST['password']) == false) {
                $this->m_formError = "Password not set";
            } else {
                $pass = $_REQUEST['password'];
                // do login
                if (!$this->login("", $pass)) {
                    $this->logout();
                }
            }
        }
        // logging out?
        if (isAction("logout") == true) {
            // just log out
            $this->logout();
            $GLOBALS['layout']->addMessage("Successfully logged out", "green");
        }
    }
    
    function isAdmin() {
        return $this->m_isAdmin;
    }
    
    function isLoggedIn() {
        return $this->m_isLoggedIn;
    }
    
    function getUsername() {
        return $this->m_username;
    }
    
    function getUserId() {
        return $this->m_userId;
    }
    
    function enforceLogin() {
        // check if logged
        if ($this->m_isLoggedIn === true) {
            return true;
        }
        // show login page
        $this->showLoginPage();
        die;
    }
    
    function terminateIfNotLoggedIn() {
        // check if logged
        if ($this->m_isLoggedIn === true) {
            return true;
        } else {
            // terminate
            die;
        }
    }
    
    function showLoginPage() {
        // check that login page is set
        if (!isset($this->m_loginPage)) {
            raiseFatalError("Account", "Login page not set");
        }

        // not logged in, prompt the login form
        $GLOBALS['layout']->printHeader();
        if (isset($this->m_formError)) {
            $GLOBALS['template']['formError'] = $this->m_formError;
        }
        include $this->m_loginPage;
        showLoginPage();
        $GLOBALS['layout']->terminatePage();
    }
}
    
?>
