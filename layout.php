<?php
/*
    Layout class
    
    NOTE:
     - set $GLOBALS["pathToBase"] if using pico from a different dir than
       the "main base path" (where the config.php resides). this is
       necessary to have all correct links to local resources.
*/

require_once __DIR__ . "/layout_item.php";
require_once __DIR__ . "/layout_menu_contents.php";
require_once __DIR__ . "/item_left_menu.php";
require_once __DIR__ . "/layout_utils.php";

class Layout {

    var $m_headerFile;
    var $m_footerFile;
    
    var $m_headerPrinted = false;
    var $m_contentsPrinted = false;
    var $m_footerPrinted = false;

    //! Root item
    var $m_root;

    //! Item with main content
    var $m_contentItem;
    
    //! Array of scripts and CSSs
    var $m_scripts; // = array( "type" ("js", "css"), "url" (opt), "inline" (opt) )
    
    //! Script to be executed when the window is loaded
    var $m_onloadJs = "";

    var $m_baseDirToPico = "";
    var $m_picoDirToBase = "";
    
    var $tpl_props; // associative array

    var $tpl_browserTitle;
    var $tpl_title;
    var $tpl_pageHeader;
    var $tpl_pageMenu;
    
    //! NOTE: $baseDirToPico holds the path of pico relative to the base URL of
    //! the served pages; this is useful for being able to link to local pico
    //! resources. $picoDirToBase holds the opposite relative path.
    function __construct($baseDirToPico, $picoDirToBase) {
        if ($baseDirToPico != "")
            $this->m_baseDirToPico = $baseDirToPico . "/";
        $this->m_picoDirToBase = __DIR__ . "/" . $picoDirToBase . "/";

        $this->m_headerFile = "style/header.php";
        $this->m_footerFile = "style/footer.php";

        $this->m_root = new LayoutItem("root", $this);
        $this->m_contentItem = $this->m_root;
        
        $this->m_scripts = array();

        // default scripts
        $this->addJavascriptLocal($this->m_baseDirToPico . "js/common.js");
        // default styles
        $this->addCssLocal($this->m_baseDirToPico . "style/style_misc.css");
        
        // other default styles
        $styles = getPicoConfig("styles");
        foreach ($styles as $style) {
            $this->addCssLocal($this->m_baseDirToPico . "/" . $style);
        }

        // default template
        $this->tpl_browserTitle = "Boe Arena";
        $this->tpl_title = "Boe Arena";
        $this->tpl_pageHeader = "?page=frame_header";
        $this->tpl_pageMenu = "style/allpages.htm";
        
        $this->tpl_props = array();
    }

    function getLayout() {
        return $this;
    }

    //! Fake function, called by the root item on construct
    function addItem($item) { }

    function root() {
        return $this->m_root;
    }

    //! Set item with main content
    function setContentItem($item) {
        $this->m_contentItem = $item;
    }

    //! Get item with main content
    function contents() {
        return $this->m_contentItem;
    }

    //! Handle special cases (like frame header, etc.)
    function handleSpecial() {
        /* OBSOLETE!
        if (isset($_GET["page"])) {
            $page = $_GET["page"];
            if ($page == "frame_header") {
                // print frame header
                $this->includePage("style/frame_header.php");
                exit;
            }
        }*/
    }

    //! Generate a query string that varies every time the resource at $url is
    //! changed, so that it is not cached by the browser
    function getResourceUrl($url) {
        $urlReal = $this->m_picoDirToBase . $url;
        return $url . "?v=" . filemtime($urlReal);
    }
    
    function getCurPage() {
        if (isset($_GET["page"])) {
            return $_GET["page"];
        } else if (isset($_GET["p"])) {
            return $_GET["p"];
        } else {
            return "home"; // default page
        }
    }

    function getLinkToPage($page) {
        return "?p=" . urlencode($page);
    }

    static function clearCache() {
        header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
    }
    
    function addMessage($mex, $color) {
        //TODO
        //echo "TODO: addMessage<br>\n";
    }
    
    function printMessages() {
        //TODO
        //echo "TODO: printMessages<br>\n";
    }
    
    private function registerScript($addBefore, $type, $url, $inline, $subdir = "") {
        if (!$addBefore)
            $this->m_scripts[] = array("type" => $type, "url" => $url, "inline" => $inline);
        else
            array_unshift($this->m_scripts, array("type" => $type, "url" => $url, "inline" => $inline));
    }
    
    function getScripts() {
        return $this->m_scripts;
    }
    
    function addScripts($scripts, $addBefore = false) {
        if (!$addBefore)
            $this->m_scripts = array_merge($this->m_scripts, $scripts);
        else
            $this->m_scripts = array_merge($scripts, $this->m_scripts);
    }
    
    function clearScripts() {
        $this->m_scripts = array();
    }
    
    //! Link to an external javascript file
    function addJavascript($url, $addBefore = false) {
        $this->registerScript($addBefore, "js", $url, "");
    }
    
    /** Link to a local javascript file (using a resource url)
        $subdir = path from directory of calling script to directory where
        the config file of pico is located, i.e. if the situation is:
            pico/layout.php ...
            config.php
            subdir/calling.php
        then $subdir = ".." (path from subdir/calling.php to config.php)
    **/
    function addJavascriptLocal($url, $addBefore = false, $subdir = "") {
        $url2 = $this->getResourceUrl($url);
        if ($subdir != "")
            $url2 = "$subdir/$url2";
        elseif (isset($GLOBALS["pathToBase"]))
            $url2 = $GLOBALS["pathToBase"] . "/$url2";
        $this->registerScript($addBefore, "js", $url2, "");
    }

    //! Add an inline script
    function addJavascriptInline($text, $addBefore = false) {
        $this->registerScript($addBefore, "js_inline", "", $text);
    }

    //! Add an inline script from file
    function addJavascriptFile($file, $addBefore = false) {
        if (file_exists($file)) {
            $contents = file_get_contents($file);
            $this->registerScript($addBefore, "js_inline", "", $contents);
        }
    }
    
    //! Add an inline script to the onload script
    function addJavascriptOnload($text, $addBefore = false) {
        if (!$addBefore)
            $this->m_onloadJs .= $text;
        else
            $this->m_onloadJs = $text . $this->m_onloadJs;
    }
    
    //! Link to an external CSS file
    function addCss($url, $addBefore = false) {
        $this->registerScript($addBefore, "css", $url, "");
    }

    //! Link to a local CSS file (using a resource url)
    function addCssLocal($url, $addBefore = false, $subdir = "") {
        $url2 = $this->getResourceUrl($url);
        if ($subdir != "")
            $url2 = "$subdir/$url2";
        elseif (isset($GLOBALS["pathToBase"]))
            $url2 = $GLOBALS["pathToBase"] . "/$url2";
        $this->registerScript($addBefore, "css", $url2, "");
    }
        
    //! Add an inline CSS
    function addCssInline($text, $addBefore = false) {
        $this->registerScript($addBefore, "css_inline", "", $text);
    }
        
    //! Add an inline CSS from file
    function addCssFile($file, $addBefore = false) {
        if (file_exists($file)) {
            $contents = file_get_contents($file);
            $this->registerScript($addBefore, "css_inline", "", $contents);
        }
    }
    
    //! Create HTML for external scripts/css
    function getScriptsHtml() {
        // add scripts
        $html = "";
        foreach ($this->m_scripts as $script) {
            switch ($script["type"]) {
                case "js":
                    $html .= "<script src=\"" . $script["url"] . "\"></script>\n";
                    break;
                case "js_inline":
                    $html .= "<script type=\"text/javascript\">\n" . $script["inline"] . "</script>\n";
                    break;
                case "css":
                    $html .= "<link rel=\"stylesheet\" href=\"" . $script["url"] . "\">\n";
                    break;
                case "css_inline":
                    $html .= "<style>\n" . $script["inline"] . "</style>\n";
                    break;
            }
        }
        // add onload script
        $html .= $this->getOnloadScriptHtml();
        return $html;
    }
    
    //! Get onload script (called by getScriptsHtml)
    private function getOnloadScriptHtml() {
        if ($this->m_onloadJs != "") {
            $html = "<script type=\"text/javascript\">\nwindow.onload = function () { "
                . $this->m_onloadJs . "}\n</script>\n";
            return $html;
        } else {
            return "";
        }
    }
    
    //! OBSOLETE
    /*function printFrameset() {
        include "style/frameset.php";
    }*/

    function includePage($page) {
        include $page;
    }

    function printPage() {
        // header
        $this->printHeader();
        // contents
        $this->printContents();
        // footer
        $this->printFooter();
    }
    
    function printHeader() {
        if ($this->m_headerPrinted == false) {
            include $this->m_headerFile;
            $this->m_headerPrinted = true;
            return true;
        } else {
            return false;
        }
    }
    
    function printContents() {
        if ($this->m_contentsPrinted == false) {
            // print root item
            $this->m_root->printContents();
            $this->m_contentsPrinted = true;
            return true;
        } else {
            return false;
        }
    }
    
    function printFooter() {
        if ($this->m_footerPrinted == false) {
            include $this->m_footerFile;
            $this->m_footerPrinted = true;
            return true;
        } else {
            return false;
        }
    }
    
    function disableHeader() {
        $this->m_headerPrinted = true;
    }
    
    function disableFooter() {
        $this->m_footerPrinted = true;
    }
    
    function disableHeaderFooter() {
        $this->disableHeader();
        $this->disableFooter();
    }
    
    function terminatePage() {
        /*// print header
        $this->printHeader();
        // print footer
        $this->printFooter();*/
        // print page as it is
        $this->printPage();
        // exit
        exit;
    }
    
    //! Refresh content of a target HTML element
    function refreshContents($elementId, $url, $justOnce) {
        if ($justOnce == true)
            $doRefresh = "false";
        else
            $doRefresh = "true";
        $script = "var node = document.getElementById('$elementId');\n"
                    . "refreshContent(node, '$url', $doRefresh);\n";
        $this->addJavascriptOnload($script);
    }
    
    //! Refresh src URL of a target HTML element
    function refreshSrc($elementId, $url, $justOnce) {
        if ($justOnce == true)
            $doRefresh = "false";
        else
            $doRefresh = "true";
        $script = "var node = document.getElementById('$elementId');\n"
                    . "refreshUrl(node, '$url', $doRefresh);\n";
        $this->addJavascriptOnload($script);
    }
    
    function setTemplateProp($key, $value) {
        $this->tpl_props[$key] = $value;
    }
    
    function getTemplateProp($key, $def = "") {
        if (isset($this->tpl_props[$key]))
            return $this->tpl_props[$key];
        else
            return $def;
    }
}

?>
