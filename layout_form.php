<?php

require_once "layout_item.php";

class LayoutFormItem extends LayoutItem {
    var $m_type = "";
    var $m_name = "";
    var $m_label = false;
    var $m_value = "";
    var $m_error = false;
    var $m_props = false;
    var $m_style = "";

    function setProps($props) {
        // skip if empty
        if ($props === false)
            return;
        // create array
        if ($this->m_props === false)
            $this->m_props = array();
        // add props
        $this->m_props = array_merge($this->m_props, $props);
        // for api
        return $this;
    }

    function setProp($key, $value = false) {
        // create array
        if ($this->m_props === false)
            $this->m_props = array();
        // set it
        $this->m_props[$key] = $value;
        // for api
        return $this;
    }

    function setStyle($style) {
        $this->m_style = $style;
        // for api
        return $this;
    }

    function addStyle($style) {
        $this->m_style .= $style;
        // for api
        return $this;
    }
    
    function getValue() {
        return $this->m_value;
    }

    function printProps() {
        // echo props
        if ($this->m_props !== false) {
            foreach ($this->m_props as $key => $val) {
                if ($val !== false)
                    echo " $key=\"$val\"";
                else
                    echo " $key";
            }
        }
    }

    static function create($parent, $typeClass, $name, $label, $initValue = false, $props = false) {
        // get init value
        // precedence to request
        if (isRequest($name)) {
            $initValue = getRequest($name, $initValue);
        }
        // check
        if ($initValue === false)
            $initValue = ""; // just set to empty string

        $item = $parent->newChild2($typeClass, $name);
        $item->m_type = $typeClass;
        $item->m_name = $name;
        $item->m_label = $label;
        $item->m_value = $initValue;
        $item->setProps($props);
        return $item;
    }
}

class LayoutFormHidden extends LayoutFormItem {
    
    function printCustomContents() {
        // text box
        echo "<input type=\"hidden\" id=\"" . $this->m_name . "\" name=\"" . $this->m_name . "\" value=\"" . htmlentities($this->m_value) . "\"";
        // dump props
        $this->printProps();
        // close
        echo " />\n";
        
        // print children
        $this->printChildren();
    }
}

class LayoutFormTextBox extends LayoutFormItem {
    var $m_renderer;
    
    //! See http://php.net/manual/en/language.types.callable.php
    function setRenderer($callback) {
        $this->m_renderer = $callback;
    }
    
    function printCustomContents() {
        // render text
        if (isset($this->m_renderer))
            $val = call_user_func($this->m_renderer, $this->m_value);
        else
            $val = $this->m_value;
        // text box
        echo "<input type=\"text\" name=\"" . $this->m_name . "\" value=\"" . htmlentities($val) . "\""
             . buildStyleStr($this->m_style);
        // dump props
        $this->printProps();

        //echo " style=\"border: 1px solid red;\"";

        // close
        echo " />\n";

        // print children
        $this->printChildren();
    }
}

class LayoutFormTextArea extends LayoutFormItem {

    /*function __construct($name, $parent) {
        parent::__construct($name, $parent);
    }*/

    function enableTabKey($useHardTabs = false) {
        if ($useHardTabs) {
            $this->setProp("onkeydown",
                "if(event.keyCode===9){var v=this.value,s=this.selectionStart,e=this.selectionEnd;this.value=v.substring(0, s)+'\t'+v.substring(e);this.selectionStart=this.selectionEnd=s+1;return false;}");
        } else {
            $this->setProp("onkeydown",
                "if(event.keyCode===9){var v=this.value,s=this.selectionStart,e=this.selectionEnd;this.value=v.substring(0, s)+'    '+v.substring(e);this.selectionStart=this.selectionEnd=s+4;return false;}");
        }
        return $this; // for api
    }

    // Ref: https://stackoverflow.com/questions/3445276/smart-text-area-auto-indent
    function enableSmartIndent() {
        $name = $this->m_name;
        $html = "<script>
        document.getElementById('$name').addEventListener('keyup', function(v){
          if(v.keyCode != 13 || v.shiftKey || v.ctrlKey || v.altKey || v.metaKey)
            return;
          var val = this.value, pos = this.selectionStart;
          var line = val.slice(val.lastIndexOf('\\n', pos - 2) + 1, pos - 1);
          var indent = /^\s*/.exec(line)[0];
          if(!indent) return;
          var st = this.scrollTop;
          this.value = val.slice(0, pos) + indent + val.slice(this.selectionEnd);
          this.selectionStart = this.selectionEnd = pos + indent.length;
          this.scrollTop = st;
        }, false);
        </script>\n";
        $this->addItemHtml($html);
        return $this; // for api
    }

    function printCustomContents() {
        // text box
        echo "<textarea id=\"" . $this->m_name . "\" name=\"" . $this->m_name . "\""
             . buildStyleStr($this->m_style);
        // dump props
        $this->printProps();
        // close
        echo ">" . htmlentities($this->m_value) . "</textarea>\n";

        // print children
        $this->printChildren();
    }
}

class LayoutFormButton extends LayoutFormItem {
    function printCustomContents() {
        // button
        echo "<input type=\"submit\" name=\"" . $this->m_name . "\" value=\"" . $this->m_value . "\""
             . buildStyleStr($this->m_style);
        // dump props
        $this->printProps();
        // close
        echo " />\n";

        // print children
        $this->printChildren();
    }
}

class LayoutFormComboBox extends LayoutFormItem {

    var $m_list = array();

    function setList($list) {
        $this->m_list = $list;
    }

    function printCustomContents() {
        echo "<select id=\"" . $this->m_name . "\" name=\"" . $this->m_name . "\""
             . buildStyleStr($this->m_style);
        // dump props
        $this->printProps();
        echo ">\n";
        // list
        foreach ($this->m_list as $key => $val) {
            if ($key == $this->m_value)
                $selected = " selected";
            else
                $selected = "";
            echo "<option value=\"$key\"$selected>" . htmlentities($val) . "</option>\n";
        }
        echo "</select>\n";

        // print children
        $this->printChildren();
    }
}

class LayoutForm extends LayoutItem {
    var $m_class = "pico_form";
    var $m_classLabel = "pico_form_label";
    var $m_styles = array();
    var $m_fields;
    var $m_method = "get";
    var $m_action = "";
    var $m_messages;

    function __construct($name, $parent) {
        parent::__construct($name, $parent);

        $this->m_fields = array();
        $this->m_messages = array();
    }

    function setMethodAction($method, $action) {
        $this->m_method = $method;
        $this->m_action = $action;
    }

    function setClass($class) {
        $this->m_class = $class;
    }

    function setTheme($index) {
        $this->m_class = "pico_navigation pico_form_theme_$index";
    }

    //! NOTE: supported objects are "table" and "div"
    function setStyle($what, $style) {
        if ($what != "table" && $what != "div") {
            raiseError("LayoutForm::setStyle()", "Unsupported object '$what' (supported are 'table' and 'div')");
        } else {
            $this->m_styles[$what] = $style;
        }
    }

    function setLabelClass($class) {
        $this->m_classLabel = $class;
    }
    
    function addHidden($name, $initValue, $props = false) {
        $item = LayoutFormItem::create($this, "LayoutFormHidden", $name, false, $initValue, $props);
        return $item;
    }

    function addTextBox($name, $label, $initValue = false, $props = false) {
        $item = LayoutFormItem::create($this, "LayoutFormTextBox", $name, $label, $initValue, $props);
        return $item;
    }

    function addTextArea($name, $label, $numRows, $numCols, $initValue = false, $props = false) {
        $props2 = array("rows" => $numRows, "cols" => $numCols);
        if ($props !== false) {
            $props2 = array_merge($props, $props2);
        }
        $item = LayoutFormItem::create($this, "LayoutFormTextArea", $name, $label, $initValue, $props2);
        return $item;
    }

    function addButton($name, $value, $props = false) {
        $item = LayoutFormItem::create($this, "LayoutFormButton", $name, false, $value, $props);
        return $item;
    }

    function addComboBox($name, $label, $list, $initValue = false, $props = false) {
        $item = LayoutFormItem::create($this, "LayoutFormComboBox", $name, $label, $initValue, $props);
        $item->setList($list);
        return $item;
    }

    /*function addField($type, $name, $label, $initValue = false, $props = false) {
        // get init value
        if ($initValue === false)
            $initValue = getRequest($name, "");
        // add field
        $this->m_fields[] = array(
            "type" => $type,
            "name" => $name,
            "label" => $label,
            "value" => $initValue,
            "error" => false,
            "props" => $props
        );
    }*/

    /*function setFieldError($name) {
        foreach ($this->m_fields as &$field) {
            if (strcmp($field->m_name, $name) == 0) {
                $field->m_error = true;
                break;
            }
        }
    }*/

    /*function setFieldProp($name, $key, $value = false) {
        foreach ($this->m_fields as &$field) {
            if (strcmp($field->m_name, $name) == 0) {
                $field->setProp($key, $value);
                break;
            }
        }
    }*/

    function addMessage($type, $str) {
        $this->m_messages[] = array("text" => $str, "type" => $type);
    }

    function addError($str) {
        $this->addMessage("error", $str);
    }

    function setFieldError($name, $str) {
        // set error to field
        $item = $this->m_items[$name];
        $item->m_error = true;
        // add string
        $this->addError($str);
    }

    function printCustomContents() {
        // outer div
        echo "<div class=\"" . $this->m_class . "\"" . buildStyleStr2($this->m_styles, "div") . ">\n";

        // messages
        if (count($this->m_messages) > 0) {
            // print a list if more than 1 message
            if (count($this->m_messages) > 1) {
                echo "<ul>\n";
                $tagType = "li";
            } else {
                // otherwise, just a paragraph
                $tagType = "p";
            }
            // list messages
            foreach ($this->m_messages as $mex) {
                $style = "";
                switch ($mex["type"]) {
                    case "error":
                        $style = "color: red";
                        break;
                    case "success":
                        $style = "color: green";
                }
                echo "<$tagType" . buildStyleStr($style) . ">" . htmlentities($mex["text"]) . "</$tagType>\n";
            }
            // close list
            if ($tagType == "li")
                echo "</ul>\n";
        }

        // form
        echo "<form method=\"" . $this->m_method . "\"" . ($this->m_action != "" ? " action=\"" . $this->m_action . "\"" : "") . ">\n".
            "<table" . buildStyleStr2($this->m_styles, "table") . ">\n";
        $rowOpened = false;
        $wasButton = false;
        //foreach ($this->m_fields as $field) {
        $count = count($this->m_itemList);
        for ($i = 0; $i < $count; ++$i) {
            $fieldIdx = $this->m_itemList[$i];
            $field = $this->m_items[$fieldIdx];

            // is this a form item?
            if (is_subclass_of($field, "LayoutFormItem")) {

                // button?
                if ($field->m_type != "LayoutFormButton") {
                    // open row
                    if ($rowOpened) {
                        if ($wasButton)
                            echo "</td>\n";
                        // close previous
                        echo "</tr>\n";
                    }
                    echo "<tr>\n";
                    $rowOpened = true;
                    // error?
                    $style = buildStyleStr($field->m_error ? "color: red" : "");
                    // build label
                    if ($field->m_label !== false) {
                        echo "<td class=\"" . $this->m_classLabel . "\"$style>" . htmlentities($field->m_label) .
                             "</td><td>";
                    } else {
                        // no label, use a td spanning 2 columns
                        echo "<td colspan=\"2\">";
                    }
                    // dump
                    $field->printContents();
                    // close
                    echo "</td>";
                    $wasButton = false;
                } else {
                    // it is a button
                    // open row if necessary
                    if ($rowOpened) {
                        if (!$wasButton) {
                            // close previous, if it was not a button
                            echo "</tr>\n";
                            // reopen
                            echo "<tr>\n";
                            echo "<td colspan=\"2\">";
                        }
                    } else {
                        // open
                        echo "<tr>\n";
                        echo "<td colspan=\"2\">";
                        $rowOpened = true;
                    }
                    // build button
                    $field->printContents();
                    $wasButton = true;
                }

            } else {
                // this item is not a form item
                // just print it
                $field->printContents();
            }
        }
        // close row
        if ($rowOpened)
            echo "</tr>\n";
        // close everything
        echo "</table>\n</form>\n</div>\n";
    }
}
