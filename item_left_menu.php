<?php

require_once "layout_item.php";

class LeftMenu extends LayoutItem {

    var $m_sections; // array of section names
    var $m_entries; // m_entries[section][item] = array("page" => page)

    function __construct($name, $parent) {
        parent::__construct($name, $parent);
        $this->m_sections = array();
        $this->m_entries = array();

        $this->encloseInDiv("container");
    }

    function addSection($name) {
        $this->m_sections[] = $name;
        $this->m_entries[$name] = array();
    }

    function addEntry($section, $name, $page) {
        // add section if not existing
        if (!in_array($section, $this->m_sections)) {
            $this->addSection($section);
        }
        // add entry
        $this->m_entries[$section][$name] = array("page" => $page);
    }
    
    function addEntryUrl($section, $name, $url) {
        // add section if not existing
        if (!in_array($section, $this->m_sections)) {
            $this->addSection($section);
        }
        // add entry
        $this->m_entries[$section][$name] = array("url" => $url);
    }

    function printCustomContents() {
        $i = 0;
        foreach ($this->m_sections as $section) {
            if ($i > 0)
                echo "<hr>\n";
            echo "<div class=\"menu\"><h3>" . htmlentities($section) . "</h3>\n";
            echo "<ul>\n";
            foreach ($this->m_entries[$section] as $name => $props) {
                if (isset($props["page"])) {
                    echo "<li><a href=\"" . $this->m_layout->getLinkToPage($props["page"]) . "\">" . htmlentities($name) . "</a></li>\n";
                } else if (isset($props["url"])) {
                    echo "<li><a href=\"" . $props["url"] . "\">" . htmlentities($name) . "</a></li>\n";
                }
            }
            echo "</ul></div>\n";
            ++$i;
        }
    }
}
