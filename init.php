<?php

require_once __DIR__ . "/config.php";
require_once __DIR__ . "/common.php";
require_once __DIR__ . "/layout.php";
require_once __DIR__ . "/layout_dialog_box.php";
require_once __DIR__ . "/account.php";
require_once __DIR__ . "/account_pass_only.php";

session_start();

$layoutClass = getPicoConfig("layoutClass");

$GLOBALS['layout'] = new $layoutClass(
    getPicoConfig("baseDirToPico"),
    getPicoConfig("picoDirToBase")
);

$accountClass = getPicoConfig("accountClass");
if ($accountClass != "")
    $GLOBALS['user'] = new $accountClass();
