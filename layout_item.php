<?php

class LayoutItem {
    //! Name
    var $m_name;

    //! Link to parent
    var $m_parent;

    //! Link to layout
    var $m_layout;

    //! Associative array of layout items
    var $m_items;

    //! List of items to parse
    var $m_itemList;

    //! HTML to include
    var $m_includeHtmlBefore;
    var $m_includeHtmlAfter;

    //! Page to include
    var $m_includePage;

    function __construct($name = "noname", $parent = false) {
        $this->m_name = $name;
        $this->m_parent = $parent;
        if ($parent !== false) {
            $this->m_layout = $parent->getLayout();
            $this->m_parent->addItem($this->m_name, $this);
        }
        $this->m_items = array();
        $this->m_itemList = array();
        $this->m_includeHtmlBefore = "";
        $this->m_includeHtmlAfter = "";
    }
    
    function setParent($parent, $doChangeName) {
        $this->m_parent = $parent;
        $this->m_layout = $parent->getLayout();
        if ($doChangeName)
            $this->m_name = $parent->getNewChildName();
        $this->m_parent->addItem($this->m_name, $this);
    }

    function name() {
        return $this->m_name;
    }

    function getLayout() {
        return $this->m_layout;
    }

    //! Get an item
    function item($name) {
        return $this->m_items[$name];
    }

    function addItem($name, $item) {
        $this->m_items[$name] = $item;
        $this->m_itemList[] = $name;
        return $item; // for api
    }

    //! Add a new child and set its HTML content
    function addItemHtml($html) {
        $item = $this->newChild();
        $item->addHtml($html);
        return $item;
    }
    
    /** Add <script> tag with local javascript file (using a resource url)
        $subdir = path from directory of calling script to directory where
        the config file of pico is located, i.e. if the situation is:
            pico/layout.php ...
            config.php
            subdir/calling.php
        then $subdir = ".." (path from subdir/calling.php to config.php)
    **/
    function addItemScriptFile($url, $addBefore = false, $subdir = "") {
        $url2 = $this->m_layout->getResourceUrl($url);
        if ($subdir != "")
            $url2 = "$subdir/$url2";
        elseif (isset($GLOBALS["pathToBase"]))
            $url2 = $GLOBALS["pathToBase"] . "/$url2";
        
        $html = "<script src=\"" . $url2 . "\"></script>\n";
        
        $item = $this->newChild();
        $item->addHtml($html);
    }
    
    function getNewChildName() {
        return $this->m_name . "_item" . count($this->m_itemList);
    }

    function newChild($name = false) {
        if ($name === false)
            return new LayoutItem($this->getNewChildName(), $this);
        else
            return new LayoutItem($name, $this);
    }
    
    function newChild2($typeClass = "LayoutItem", $name = false) {
        if ($name === false)
            return new $typeClass($this->getNewChildName(), $this);
        else
            return new $typeClass($name, $this);
    }

    function addHtmlBefore($text) {
        $this->m_includeHtmlBefore .= $text;
        return $this; // for api
    }

    function addHtmlAfter($text) {
        $this->m_includeHtmlAfter .= $text;
        return $this; // for api
    }
    
    function addHtml($text) {
        //$this->addHtmlAfter($text);
        $this->addHtmlBefore($text);
        return $this; // for api
    }

    function clearHtml() {
        $this->m_includeHtmlBefore = "";
        $this->m_includeHtmlAfter = "";
        return $this; // for api
    }

    // NOTE: call "encloseIn..." BEFORE adding the HTML enclosed!
    function encloseInHtml($pre, $post) {
        $this->m_includeHtmlBefore .= $pre;
        $this->m_includeHtmlAfter = $post . $this->m_includeHtmlAfter;
        //$this->m_includeHtmlBefore = $pre . $this->m_includeHtmlBefore;
        //$this->m_includeHtmlAfter = $this->m_includeHtmlAfter . $post;
        return $this; // for api
    }

    function encloseInTag($tag, $props = false) {
        $propStr = "";
        if ($props !== false) {
            foreach ($props as $key => $val) {
                $propStr .= " " . $key . "=\"" . $val . "\"";
            }
        }
        $this->encloseInHtml("<" . $tag . $propStr . ">\n", "</" . $tag . ">\n");
        return $this; // for api
    }

    function encloseInDiv($class = "", $id = "") {
        $props = array();
        if (strlen($class) > 0)
            $props["class"] = $class;
        if (strlen($id) > 0)
            $props["id"] = $id;
        $this->encloseInTag("div", $props);
        return $this; // for api
    }

    function setAsJavascript($script) {
        $this->encloseInTag("script", array("type" => "text/javascript"));
        $this->addHtmlBefore($script);
        return $this; // for api
    }

    function setAsJavascriptLocal($filename) {
        $this->encloseInTag("script", array("src" => $this->m_layout->getResourceUrl($filename)));
        return $this; // for api
    }

    function addPage($page) {
        $this->m_includePage = $page;
        return $this; // for api
    }

    function printContents() {
        // echo text
        echo $this->m_includeHtmlBefore;
        // add page
        if (isset($this->m_includePage)) {
            $this->m_layout->includePage($this->m_includePage);
        }
        // print custom contents
        $this->printCustomContents();
        // echo text
        echo $this->m_includeHtmlAfter;
    }
    
    function printChildren() {
        // parse each item, in order
        $count = count($this->m_itemList);
        for ($i = 0; $i < $count; ++$i) {
            // select item
            $itemIdx = $this->m_itemList[$i];
            $this->m_items[$itemIdx]->printContents();
        }
    }

    function printCustomContents() {
        // print stuff...
        // print children
        $this->printChildren();
    }
}

