<?php

function addShowHideItem($parent, $title) {
    $item = $parent->newChild();

    $aId = $item->name();
    $divId = $item->name() . "_subDiv";
    $item->encloseInTag("div", array("style" => "display: block"));
    $item->addHtmlBefore("<p><a id=\"$aId\" style=\"text-decoration: underline\">$title</a></p>");
    $item->encloseInTag("div", array("id" => $divId, "style" => "display: none"));
    $item2 = $item->newChild();

    //$item2->addHtml("SUB");

    $script = "<script>\n".
        "$(function() {\n".
        "$('#${aId}').click(function() {\n".
        "$('#${divId}').toggle();\n".
        "});\n".
        "});\n".
        "</script>\n";
    $item->addHtmlAfter($script);

    return $item2;
}

