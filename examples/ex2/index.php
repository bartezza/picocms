<?php

// initialize pico
$GLOBALS["config_file"] = ""; // use default config
require_once __DIR__ . "/pico/init.php";


// change the error handler class
class MyErrorHandler extends ErrorHandler {

    function raiseWarning($title, $str) {
        $this->createMessageBox("warning_box", $title . " (MODIFIED)", $str);
    }
    
    function raiseError($title, $str) {
        $this->createMessageBox("error_box", $title . " (MODIFIED)", $str);
    }
    
    function raiseFatalError($title, $str) {
        $this->raiseError($title, $str);
        $GLOBALS["layout"]->terminatePage();
        die;
    }
}

// create the error handler
createErrorHandler("MyErrorHandler");

// title
$layout->contents()->addItemHtml("<h2>Example 2: some warnings and errors</h2>");

// raise some errors
raiseWarning("Title", "This is a warning");

raiseError("Title", "This is an error");

raiseFatalError("Title", "This is a FATAL ERROR!");

