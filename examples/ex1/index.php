<?php

// initialize pico
require_once __DIR__ . "/config.php";
require_once __DIR__ . "/pico/init.php";
require_once __DIR__ . "/pico/item_left_menu.php";

// create a structure with title, menu, contents
$structure = $layout->root()->newChild2("LayoutMenuContents");

// add a title to the title div
$structure->title()->addHtml("<h1><a>This is the title</a></h1>");

// add a menu to the menu div
$menu = $structure->menu()->newChild2("LeftMenu");

// populate the menu with some sections and items
for ($i = 0; $i < 2; ++$i) {
    for ($j = 0; $j < 3; ++$j) {
        $menu->addEntry("Section $i", "Item $j", "item_${i}_${j}");
    }
}

// get contents div
$contents = $structure->contents();

// get requested page
$page = getRequest("p", "home");

// explode the page
$items = explode("_", $page);

// do we have a valid sub-page?
if (count($items) == 3) {
    // yes, display it
    $contents->addItemHtml("<h2>Section " . $items[1] . "</h2>");
    $contents->addItemHtml("<h3>Item " . $items[2] . "</h3>");
    
    for ($i = 0; $i < 30; ++$i) {
        $contents->addItemHtml("<p>bla bla bla bla</p>");
    }
} else {
    // no, just display the home page
    $contents->addItemHtml("<h2>Home</h2>");

    for ($i = 0; $i < 30; ++$i) {
        $contents->addItemHtml("<p>bla bla bla bla</p>");
    }
}

// flush the page and terminate
$layout->terminatePage();
