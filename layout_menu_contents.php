<?php

require_once "layout_item.php";

//! Layout with three sections: title, menu, contents
class LayoutMenuContents extends LayoutItem {
    var $m_parent;

    var $m_title;
    var $m_titleUsed;
    var $m_page;
    var $m_menu;
    var $m_contents;

    function __construct($name, $parent) {
        parent::__construct($name, $parent);

        $this->createTitle();
        $this->m_titleUsed = false;

        $this->createPage();
        $this->createMenu();
        $this->createContents();
    }

    function createTitle() {
        $this->m_title = new LayoutItem("title", $this);
        //$this->m_title->encloseInDiv("", "title"); // set when used!
    }

    function createPage() {
        $this->m_page = new LayoutItem("page", $this);
        $this->m_page->encloseInDiv("", "page");
    }

    function createMenu() {
        $this->m_menu = new LayoutItem("menu", $this->m_page);
        $this->m_menu->encloseInDiv("", "menu");
    }

    function createContents() {
        $this->m_contents = new LayoutItem("contents", $this->m_page);
        $this->m_contents->encloseInDiv("", "contents");

        $this->getLayout()->setContentItem($this->m_contents);
    }

    function title() {
        // have we already used the title?
        if (!$this->m_titleUsed) {
            // ok, we have to change the contents style to account for the title
            // TODO: do this better!!!
            $this->m_contents->clearHtml();
            // make space for the title setting absolute top position to 100px
            $this->m_contents->encloseInTag("div", array("id" => "contents", "style" => "top: 100px"));
            
            // change the style of the title too
            $this->m_title->encloseInTag("div", array("id" => "title")); //, "style" => "top: 100px"));

            $this->m_titleUsed = true;
        }
        return $this->m_title;
    }

    function menu() {
        return $this->m_menu;
    }

    function contents() {
        return $this->m_contents;
    }
}
