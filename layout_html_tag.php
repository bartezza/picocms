<?php
/*
    TODO: make this the base class of layout form items and layout form
*/

require_once "layout_item.php";

class LayoutHtmlTag extends LayoutItem {
    var $m_class = false;
    var $m_style = array();
    var $m_props = array();
    var $m_tag = "";

    function __construct($name, $parent, $tag = "div") {
        parent::__construct($name, $parent);
        $this->m_tag = $tag;
    }
    
    function setTag($tag) {
        $this->m_tag = $tag;
        return $this;
    }

    function setClass($class) {
        $this->m_class = $class;
        return $this;
    }

    function setStyle($what, $style) {
        $this->m_style[$what] = $style;
        return $this;
    }
    
    function unsetStyle($what) {
        unset($this->m_style[$what]);
    }
    
    function setProps($props) {
        // skip if empty
        if ($props === false)
            return;
        // create array
        if ($this->m_props === false)
            $this->m_props = array();
        // add props
        $this->m_props = array_merge($this->m_props, $props);
        // for api
        return $this;
    }

    function setProp($key, $value = false) {
        // create array
        if ($this->m_props === false)
            $this->m_props = array();
        // set it
        $this->m_props[$key] = $value;
        // for api
        return $this;
    }
    
    function printProps() {
        // echo props
        if ($this->m_props !== false) {
            foreach ($this->m_props as $key => $val) {
                if ($val !== false)
                    echo " $key=\"$val\"";
                else
                    echo " $key";
            }
        }
    }

    function printCustomContents() {
        // open
        echo "<" . $this->m_tag;
        // props
        $this->printProps();
        // class
        echo buildClassStr($this->m_class);
        // style
        if (count($this->m_style) > 0) {
            echo " style=\"";
            foreach ($this->m_style as $key => $val) {
                echo "$key: $val;";
            }
            echo "\"";
        }
        // end of open
        echo ">\n";
        // print children
        $this->printChildren();
        // close
        echo "</" . $this->m_tag . ">\n";
    }
}

