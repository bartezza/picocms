<?php
/*
    Init script for services (e.g. ajax)
*/

require_once __DIR__ . "/config.php";
require_once __DIR__ . "/common.php";
require_once __DIR__ . "/layout.php";
require_once __DIR__ . "/account.php";

session_start();

$GLOBALS['user'] = new User();

$GLOBALS['user']->terminateIfNotLoggedIn();

