<?php
/*
    Common functions

    Exported functions:
     - bool isAction($actionName)
*/

require_once __DIR__ . "/config.php";
require_once __DIR__ . "/utils/php_errors.php";
require_once __DIR__ . "/utils/utils.php";
require_once __DIR__ . "/utils/errors.php";
require_once __DIR__ . "/utils/db.php";

?>
