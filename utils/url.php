<?php
/*
    Url class

    Exported functions:
     - setBaseUrl($baseUrl)
     - addParam($name, $value)
     - string getUrl()
*/
    
class Url {
    
    var $m_baseUrl;
    var $m_params;
    
    function __construct() {
        $this->m_baseUrl = "#";
        $this->m_params = array();
    }
    
    function setBaseUrl($baseUrl) {
        $this->m_baseUrl = $baseUrl;
    }
    
    function addParam($name, $value) {
        $this->m_params[] = array($name, $value);
    }
    
    function getUrl() {
        $str = $this->m_baseUrl;
        if (count($this->m_params) > 0) {
            $str .= "?" . $this->m_params[0][0] . "=" . urlencode($this->m_params[0][1]);
            for ($i = 1; $i < count($this->m_params); $i++) {
                $str .= "&" . $this->m_params[$i][0] . "=" . urlencode($this->m_params[$i][1]);
            }
        }
        return $str;
    }
}
    
?>