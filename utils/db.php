<?php

require_once __DIR__ . "/php_errors.php";
require_once __DIR__ . "/errors.php";
require_once __DIR__ . "/../config.php";

class DBManager {

    private $m_pdo = null;

    function __construct() {
    
    }
    
    function connect() {
        // check if already connected
        if ($this->m_pdo !== null)
            return;
        
        // get settings
        $mysqlSettinsg = getPicoSettings("mysqlConfig");
 
        // connnect
        try {
            $this->m_pdo = new PDO("mysql:host=" . $mysqlSettings["host"] . ";dbname=" . $mysqlSettings["db"] . ";charset=utf8",
                                   $mysqlSettings["username"], $mysqlSettings["password"],
                                   array(PDO::ATTR_EMULATE_PREPARES => false, PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        } catch (PDOException $e) {
            raiseFatalError("mysql", $e->getMessage());
        }
    }

    public function __destruct(){
        if ($this->m_pdo)
            $this->m_pdo = null;
    }

    public function quote($str) {
        connect();
        return $this->m_pdo->quote($str);
    }

    public function query($query) {
        connect();
        return $this->m_pdo->query($query);
    }

    public function queryFetchAll($query) {
        connect();
        $result = $this->m_pdo->query($query, PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }

    public function prepare($query) {
        connect();
        $statement = $this->m_pdo->prepare($query);
        return $statement;
    }

    // insert rows into table from associative array
    public function insertRows($table, $values) {
        connect();
        if (sizeof($values) > 0) {
            $keys = array_keys($values[0]);
            $packed_hooks = array();
            $packed_values = array();
            foreach ($values as $index => $row) {
                //if (!array_diff_key(array_keys($row), $keys)) {
                    // build array of keys with the correct order
                    $indexed_keys = array();
                    foreach ($keys as $key_index => $key) {
                        $indexed_keys[$key_index] = ":" . $key . $index;
                        $packed_values[":" . $key . $index] = $row[$key];
                    }
                    $packed_hooks[] = "(" . implode(",", $indexed_keys) . ")";
                //}
            }
            $prepared_statement = "INSERT IGNORE INTO {$table} (" . implode(",", $keys) . ") VALUES "
                . implode(", ", $packed_hooks);
            $statement = $this->m_pdo->prepare($prepared_statement);
            return $statement->execute($packed_values);
        }
        return false;
    }

    public function insertRow($table, $values) {
        connect();
        $arr_values[0] = $values;
        return $this->insertRows($table, $arr_values);
    }

}

