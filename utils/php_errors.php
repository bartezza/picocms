<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (!function_exists("errHandle")) {
    function errHandle($errNo, $errStr, $errFile, $errLine) {
        $msg = "$errStr in $errFile on line $errLine";
        echo "<pre>\n";
        //if ($errNo == E_NOTICE || $errNo == E_WARNING) {
            throw new ErrorException($msg, $errNo);
        /*} else {
            echo $msg;
        }*/
    }

    //set_error_handler('errHandle');

    function exception_error_handler($errno, $errstr, $errfile, $errline ) {
        //echo "<pre>\n";
        throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
    }

    set_error_handler("exception_error_handler");

    function exception_handler($exception) {
        /*echo "<pre style=\"white-space: pre-wrap;\"><b>Uncaught exception:</b><br>" . $exc->getMessage() . "</pre>\n";
        echo "<pre style=\"white-space: pre-wrap;\">";
        debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        echo "</pre>\n";*/
        echo '<div class="alert alert-danger">';
        echo '<b>Fatal error</b> - Uncaught exception \'<b>' . get_class($exception) . '\'</b> with message:<br><br><b>';
        echo $exception->getMessage() . '</b><br><br>';
        echo "Stack trace:<pre style=\"white-space: pre-wrap;\">" . $exception->getTraceAsString() . '</pre>';
        echo 'thrown in <b>' . $exception->getFile() . '</b> on line <b>' . $exception->getLine() . '</b><br>';
        echo '</div>';
    }

    function raiseFatalErrorException($exception) {
        /*echo "<pre style=\"white-space: pre-wrap;\"><b>Uncaught exception:</b><br>" . $exc->getMessage() . "</pre>\n";
        echo "<pre style=\"white-space: pre-wrap;\">";
        debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        echo "</pre>\n";*/

        $html = '<b>Fatal error</b> - Uncaught exception \'<b>' . get_class($exception) . '\'</b> with message:<br><br><b>';
        $html .= $exception->getMessage() . '</b><br><br>';
        $html .= "Stack trace:<pre style=\"white-space: pre-wrap;\">" . $exception->getTraceAsString() . '</pre>';
        $html .= 'thrown in <b>' . $exception->getFile() . '</b> on line <b>' . $exception->getLine() . '</b><br>';
        raiseFatalError("Exception", $html, true);
    }

    set_exception_handler("exception_handler");
}
