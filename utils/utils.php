<?php

function isAction($actionName) {
    $actionName2 = "do_" . $actionName;
    if (isset($_REQUEST[$actionName2]) == true && $_REQUEST[$actionName2] == "1")
        return true;
    else
        return false;
}

function getFieldOrFalse($arr, $field) {
    if ($arr === false)
        return false;
    if (!isset($arr[$field]))
        return false;
    else
        return $arr[$field];
}

function isRequest($key) {
    return isset($_REQUEST[$key]);
}

function getRequest($key, $default) {
    if (isset($_REQUEST[$key]))
        return $_REQUEST[$key];
    else
        return $default;
}

function buildStyleStr($style) {
    if ($style == "" || $style === false || $style == null)
        return "";
    else
        return " style=\"$style\"";
}

//! Build style str from array of styles
//! e.g. $array["table"]
function buildStyleStr2($array, $what) {
    if (!is_array($array) || !isset($array[$what]))
        return "";
    $style = $array[$what];
    if ($style == "" || $style === false || $style == null)
        return "";
    else
        return " style=\"$style\"";
}

function combineStyleStr($style1, $style2) {
    if ($style1 == "")
        return $style2;
    if ($style2 == "")
        return $style1;
    $str = trim($style1);
    if (substr($str, -1) != ";")
        $str .= ";";
    $str .= $style2;
    return $str;
}

function buildClassStr($class) {
    if ($class == "" || $class === false || $class == null)
        return "";
    else
        return " class=\"$class\"";
}

//! Build class str from array of classes
//! e.g. $array["table"]
function buildClassStr2($array, $what) {
    if (!is_array($array) || !isset($array[$what]))
        return "";
    $class = $array[$what];
    if ($class == "" || $class === false || $class == null)
        return "";
    else
        return " class=\"$class\"";
}

//! Generate random token
function generateToken($numBytes = 32) {
    return bin2hex(openssl_random_pseudo_bytes($numBytes / 2));
}

//! Check if given arg contains a boolean
function isBoolean($str) {
    if (is_bool($str) || $str == 0 || $str == 1)
        return true;
    $str = strtolower($str);
    if ($str == "true" || $str == "false" || $str == "yes" || $str == "no")
        return true;
    return false;
}

//! Get boolean value
function getBoolean($str) {
    if (is_bool($str))
        return $str;
    else if ($str === 0)
        return false;
    else if ($str === 1)
        return true;
    $str = strtolower($str);
    if (strcmp($str, "true") == 0 || strcmp($str, "yes") == 0)
        return true;
    else if (strcmp($str, "false") == 0 || strcmp($str, "no") == 0)
        return false;
    else {
        raiseError("Error", "Invalid boolean '$str'");
        return false; // treat everything else as false
    }
}
