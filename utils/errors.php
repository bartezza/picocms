<?php

require_once __DIR__ . "/php_errors.php";

//! Default error handler class
class ErrorHandler {

    protected function createMessageBox($class, $title, $message, $isHtml, $detailsItem = false) {
        /*$item = $GLOBALS["layout"]->contents()->newChild();
        $item->encloseInTag("div", array("class" => $class));
        $item->addHtml("<h2>" . htmlentities($title) . "</h2>\n<p class=\"$class\">" . htmlentities($message) . "</p>");*/
        $item = $GLOBALS["layout"]->contents()->newChild2("LayoutDialogBox");
        $item->setClass($class);
        $item->setTitle($title);
        $item->addItemHtml("" . ($isHtml ? $message : htmlentities($message)) . "");
        
        // add details
        if ($detailsItem !== false)
            $detailsItem->setParent($item, false);
    }

    function raiseWarning($title, $str, $isHtml) {
        $this->createMessageBox("warning_box", $title, $str, $isHtml);
    }
    
    function raiseError($title, $str, $isHtml) {
        $this->createMessageBox("error_box", $title, $str, $isHtml);
    }
    
    function raiseFatalError($title, $str, $isHtml) {
        $this->createMessageBox("error_box", $title, $str, $isHtml);
        $GLOBALS["layout"]->terminatePage();
        die;
    }
}


function createErrorHandler($className) {
    $GLOBALS["error_handler"] = new $className();
}

function raiseWarning($title, $str, $isHtml = false, $detailsItem = false) {
    $GLOBALS["error_handler"]->raiseWarning($title, $str, $isHtml, $detailsItem);
}

function raiseError($title, $str, $isHtml = false, $detailsItem = false) {
    $GLOBALS["error_handler"]->raiseError($title, $str, $isHtml, $detailsItem);
}

function raiseFatalError($title, $str, $isHtml = false, $detailsItem = false) {
    $GLOBALS["error_handler"]->raiseFatalError($title, $str, $isHtml, $detailsItem);
}

// create the initial error handler class
if (!isset($GLOBALS["error_handler"])) {
    createErrorHandler("ErrorHandler");
}

