<?php
/*
    Password-only accounts
*/

require_once "account.php";

class UserPassOnly extends User {

    function __construct() {
        parent::__construct();

        $this->m_loginPage = __DIR__ . "/login_page_pass_only.php";
    }

    function login($username, $pass) {
        // check against all users
        $users = getPicoConfig("users");
        foreach ($users as $user) {
            // check password
            $found = false;
            if (!isset($user["hashed"]) || $user["hashed"] == false) {
                if (strcmp($user["password"], $pass) == 0) {
                    $found = true;
                }
            } else {
                if (password_verify($pass, $user["password"])) {
                    $found = true;
                }
            }
            // login
            if ($found) {
                $this->m_username = $user["username"];
                $this->m_userId = $user["userId"];
                $this->m_isAdmin = $user["isAdmin"];
                $this->m_isLoggedIn = true;
                // update session
                $this->updateSession();
                return true;
            }
        }
        $this->m_formError = "Invalid password";
        return false;
    }

}

