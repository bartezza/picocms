<?php

// https://www.sitepoint.com/interactive-cli-password-prompt-in-php/

function prompt_silent($prompt = "Enter password: ") {
  if (preg_match('/^win/i', PHP_OS)) {
    $vbscript = sys_get_temp_dir() . 'prompt_password.vbs';
    file_put_contents(
      $vbscript, 'wscript.echo(InputBox("'
      . addslashes($prompt)
      . '", "", "password here"))');
    $command = "cscript //nologo " . escapeshellarg($vbscript);
    $password = rtrim(shell_exec($command));
    unlink($vbscript);
    return $password;
  } else {
    $command = "/usr/bin/env bash -c 'echo OK'";
    if (rtrim(shell_exec($command)) !== 'OK') {
      trigger_error("Can't invoke bash");
      return false;
    }
    $command = "/usr/bin/env bash -c 'read -s -p \""
      . addslashes($prompt)
      . "\" mypassword && echo \$mypassword'";
    $password = rtrim(shell_exec($command));
    echo "\n";
    return $password;
  }
}

$pass = prompt_silent();

if ($pass !== false) {
    $passHash = password_hash($pass, PASSWORD_DEFAULT);
    
    echo $passHash . "\n";
    
    echo "\naddPicoUserHashed(\"0\", \"user\", \"$passHash\", false);\n";
}

