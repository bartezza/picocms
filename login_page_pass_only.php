<?php
/*
    Login HTML block

    Global variables:
     - $GLOBALS['template']['formError']
*/

function showLoginPage() {?>
<div>
<h3>Restricted area</h3>
<form method="post" action="?do_login=1">
    <?php
    if (isset($GLOBALS['template']['formError']) && $GLOBALS['template']['formError'] != "") {
        echo "<font style=\"color: red\">" . $GLOBALS['template']['formError'] . "</font><br><br>\n";
    }
    ?>
    Password:
    &nbsp;&nbsp;&nbsp;&nbsp;<input type="password" name="password" value="">
    &nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="submit" value="Ok"><br>
</form>
</div>
<?php
}
