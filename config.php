<?php
/*
    Configuration file
*/

// default things
$GLOBALS["pico_config"] = array(
    // layout class to create
    "layoutClass" => "Layout",
    
    // default style files
    // NOTE: these are relative to pico base directory ("baseDirToPico" will be
    // prepended!)
    "styles" => array(
        "style/style_blocks.css"
    ),
    
    // account class to create
    "accountClass" => "UserPassOnly",
    
    "baseDirToPico" => "pico", // assuming by default pico to be in a subdir
    "picoDirToBase" => "..",
    "mysqlConfig" => array(
        "host" => "host",
        "db" => "db",
        "username" => "username",
        "password" => "password"
    ),
    "users" => array(
        array( "username" => "admin", "password" => "admin", "hashed" => false, "isAdmin" => true, "userId" => 0 ),
        array( "username" => "guest", "password" => "guest", "hashed" => false, "isAdmin" => false, "userId" => 1 )
    )
);

// load custom config
if (isset($GLOBALS["config_file"])) {
    // if it is "" then do not load a custom config, just use the default one
    if ($GLOBALS["config_file"] != "")
        require_once $GLOBALS["config_file"];
} else {
    echo "Config file not set!"; exit;
    //require_once __DIR__ . "/../config.php";
}

// get config
function getPicoConfig($key) {
    return $GLOBALS["pico_config"][$key];
}

//! NOTE: this can be used as setPicoConfig($key, $val) or as
//! setPicoConfig($assocArray)
function setPicoConfig($key, $val = false) {
    if (is_array($key)) {
        foreach ($key as $theKey => $theVal) {
            $GLOBALS["pico_config"][$theKey] = $theVal;
        }
    } else {
        $GLOBALS["pico_config"][$key] = $val;
    }
}

function clearPicoStyles() {
    $GLOBALS["pico_config"]["styles"] = array();
}

function addPicoStyle($filename) {
    $GLOBALS["pico_config"]["styles"][] = $filename;
}

function clearPicoUsers() {
    $GLOBALS["pico_config"]["users"] = array();
}

function addPicoUser($userId, $username, $password, $isAdmin) {
    $GLOBALS["pico_config"]["users"][] = array(
        "username" => $username, "password" => $password, "hashed" => false, "isAdmin" => $isAdmin, "userId" => $userId
    );
}

function addPicoUserHashed($userId, $username, $passwordHash, $isAdmin) {
    $GLOBALS["pico_config"]["users"][] = array(
        "username" => $username, "password" => $passwordHash, "hashed" => true, "isAdmin" => $isAdmin, "userId" => $userId
    );
}

