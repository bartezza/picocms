<?php

require_once "layout_item.php";

class LayoutDialogBox extends LayoutItem {
    var $m_parent;

    var $m_title;
    var $m_class;

    function __construct($name, $parent) {
        parent::__construct($name, $parent);
    }
    
    function setTitle($title) {
        $this->m_title = $title;
    }

    function setClass($class) {
        return $this->m_class = $class;
    }

    function printCustomContents() {
        // open div
        $class = "";
        if (isset($this->m_class))
            $class = " " . $this->m_class;
        echo "<div class=\"dialog_box" . $class . "\">\n";
        // print title
        if (isset($this->m_title))
            echo "<h2>" . htmlentities($this->m_title) . "</h2>\n";
        // inner div
        echo "<div class=\"dialog_box_cont\">\n";
        // print children
        $this->printChildren();
        // close div
        echo "</div>\n";
        echo "</div>\n";
    }
}
