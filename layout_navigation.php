<?php

require_once "layout_item.php";

class LayoutNavigation extends LayoutItem {
    var $m_class;
    var $m_divStyle = "";
    var $m_ulStyle = "";
    var $m_liStyle = "";
    var $m_liStyle2 = "";
    var $m_aStyle = "";
    var $m_entries;

    function __construct($name, $parent) {
        parent::__construct($name, $parent);

        $this->m_class = "pico_navigation pico_nav_theme1";
        $this->setOnTop("0px");
        $this->m_entries = array();
    }

    function setClass($class) {
        $this->m_class = $class;
    }

    function setOnTop($topPixels = "0px", $otherStyle = "") {
        $this->setDivStyle("position: fixed; top: $topPixels; width: 100%;$otherStyle");
    }

    function setTheme($index) {
        $this->m_class = "pico_navigation pico_nav_theme_$index";
    }

    function setDivStyle($style) {
        $this->m_divStyle = ($style != "" ? " style=\"$style\"" : "");
    }

    function setUlStyle($style) {
        $this->m_ulStyle = ($style != "" ? " style=\"$style\"" : "");
    }

    function setLiStyle($style) {
        $this->m_liStyle2 = $style;
        $this->m_liStyle = ($style != "" ? " style=\"$style\"" : "");
    }

    function setAnchorStyle($style) {
        $this->m_aStyle = $style;
    }

    function setEntryStyle($name, $style) {
        foreach ($this->m_entries as &$entry) {
            if ($entry["name"] == $name) {
                //$entry["onTheRight"] = true;
                $entry["style"] = combineStyleStr($entry["style"], $style);
                break;
            }
        }
    }

    function setEntryAnchorStyle($name, $style) {
        foreach ($this->m_entries as &$entry) {
            if ($entry["name"] == $name) {
                //$entry["onTheRight"] = true;
                $entry["style_anchor"] = combineStyleStr($entry["style_anchor"], $style);
                break;
            }
        }
    }

    function addEntry($title, $name, $url, $style = "") {
        $this->m_entries[] = array("title" => $title, "name" => $name, "url" => $url, "active" => false, "style" => $style, "style_anchor" => "");
    }

    function setActive($name) {
        foreach ($this->m_entries as &$entry) {
            if ($entry["name"] == $name) {
                $entry["active"] = true;
                break;
            }
        }
    }

    function setOnTheRight($name) {
        $this->setEntryStyle($name, "float: right;");
    }

    function printCustomContents() {
        echo "<div class=\"" . $this->m_class . "\"" . $this->m_divStyle . ">\n<ul" . $this->m_ulStyle . ">\n";
        foreach ($this->m_entries as $entry) {
            $active = ($entry["active"] ? " class=\"active\"" : "");

            $liStyle2 = combineStyleStr($this->m_liStyle, $entry["style"]);
            $liStyle = buildStyleStr($liStyle2);

            $aStyle2 = combineStyleStr($this->m_aStyle, $entry["style_anchor"]);
            $aStyle = buildStyleStr($aStyle2);

            echo "<li" . $liStyle . "><a" . $aStyle . "$active href=\"" . $entry["url"] . "\">" . htmlentities($entry["title"]) . "</a></li>\n";
        }
        echo "</ul>\n</div>\n";
    }
}
